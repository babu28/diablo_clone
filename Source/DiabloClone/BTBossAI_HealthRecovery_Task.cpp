// Fill out your copyright notice in the Description page of Project Settings.

#include "BTBossAI_HealthRecovery_Task.h"
#include "Components/ProgressBar.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "particles/ParticleSystemComponent.h"

UBTBossAI_HealthRecovery_Task::UBTBossAI_HealthRecovery_Task()
{
	bNotifyTick = true;
}

EBTNodeResult::Type UBTBossAI_HealthRecovery_Task::ExecuteTask(UBehaviorTreeComponent& owner, uint8* node)
{
	timer = 0.0f;

	BTParent = (AEnemy_Boss_AI_Controller*)(owner.GetAIOwner());
	AIPawn = BTParent->GetPawn();
	boss_Health = BTParent->CharClass->getCurrentBossHealth();
	boss_Health = boss_Health / BTParent->CharClass->boss_MaxHealth;
	BTParent->isRecoveringHealth = true;

	if (USkeletalMeshComponent* Mesh = AIPawn->FindComponentByClass<USkeletalMeshComponent>())
	{
		if (UAnimInstance* AnimInst = Mesh->GetAnimInstance())
		{
			AIAnimator = AnimInst;
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBasicAttack");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
			}
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canLightningStrike");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
			}
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "isHealthRecovery");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, true);
			}
		}
	}

	spawnedParticle = UGameplayStatics::SpawnEmitterAtLocation(GetWorld() , BTParent->CharClass->healthRegenParticle , BTParent->CharClass->areanCenterPos);
	spawnedParticle->SetWorldScale3D(FVector(4.0f, 4.0f, 4.0f));
	AIPawn->SetActorLocation(BTParent->CharClass->areanCenterPos);
	return EBTNodeResult::InProgress;
}

void UBTBossAI_HealthRecovery_Task::TickTask(UBehaviorTreeComponent& owner, uint8* node, float DeltaSeconds)
{
	boss_Health = boss_Health + (DeltaSeconds * 0.15f);

	if (boss_Health > 1.0f)
	{
		BTParent->CharClass->ProgressBar->SetPercent(1.0f);
		BTParent->isRecoveringHealth = false;
		BTParent->blackBoardCompoenent->SetValueAsBool(TEXT("isHalfHealth"), false);
		BTParent->CharClass->setBossHealth(BTParent->CharClass->boss_MaxHealth);
		BTParent->setBossHealth(BTParent->CharClass->boss_MaxHealth);

		MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "isHealthRecovery");
		if (MyBoolProp != NULL)
		{
			MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
		}
		spawnedParticle->DestroyComponent();
		spawnedParticle = nullptr;
		//returns the control
		FinishLatentTask(owner, EBTNodeResult::Succeeded);
	}

	BTParent->CharClass->ProgressBar->SetPercent(boss_Health);
}