// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemy_Boss_AI.generated.h"

UCLASS()
class DIABLOCLONE_API AEnemy_Boss_AI : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy_Boss_AI();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TakeDamageToHealth(float Damage);

	float getCurrentBossHealth();

	void setBossHealth(float health);
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* healthRegenParticle;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* beam;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* beamHeadSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* beamHeadGlow;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* lightningStrikeParticle1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* lightningStrikeParticle2;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Particles")
		class UParticleSystem* strikeAreaParticle;

	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UWidgetComponent* healthBarWidget;
	UPROPERTY()
		class UProgressBar* ProgressBar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Health")
		float boss_MaxHealth = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Area Position")
		AActor* arenaCenter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Area Position")
		AActor* beamStart;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Area Position")
		AActor* beamEnd;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Area Position")
		TArray<AActor*> lightningStrikePos;

	//can be done with getlocation() insted of a saperate varible
	FVector areanCenterPos;

protected:
	float boss_Health = 2.0f;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	bool isDead = false;
	float deadWaitTimer = 0.0f;
};
