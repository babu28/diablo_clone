// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "Enemy_Boss_AI_Controller.h"
#include "BTBossAI_HealthRecovery_Task.generated.h"

/**
 * 
 */
UCLASS()
class DIABLOCLONE_API UBTBossAI_HealthRecovery_Task : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public:
	UBTBossAI_HealthRecovery_Task();
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& owner, uint8* node) override;
	virtual void TickTask(UBehaviorTreeComponent& owner, uint8* node, float DeltaSeconds) override;

private:
	UAnimInstance* AIAnimator = nullptr;
	AEnemy_Boss_AI_Controller* BTParent = nullptr;
	APawn* AIPawn = nullptr;
	FBoolProperty* MyBoolProp = nullptr;
	UParticleSystemComponent* spawnedParticle = nullptr;
	FQuat rotation;
	float boss_Health = 0.0f;
	float timer = 0.0f;
	
};
