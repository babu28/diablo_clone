// Fill out your copyright notice in the Description page of Project Settings.

#include "BTBossAI_LightningStrike_Task.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "particles/ParticleSystemComponent.h"
#include "DrawDebugHelpers.h"

UBTBossAI_LightningStrike_Task::UBTBossAI_LightningStrike_Task()
{
	bNotifyTick = true;
}

EBTNodeResult::Type UBTBossAI_LightningStrike_Task::ExecuteTask(UBehaviorTreeComponent& owner, uint8* node)
{
	timer = 0.0f;
	spawnedParticleStrikeArea.Empty();
	spawnedParticleLightning1.Empty();
	spawnedParticleLightning2.Empty();
	hasCastLightning = false;
	isHit = false;
	

	BTParent = (AEnemy_Boss_AI_Controller*)(owner.GetAIOwner());
	AIPawn = BTParent->GetPawn();
	Mesh = AIPawn->FindComponentByClass<USkeletalMeshComponent>();
	objectTypesArray.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));
	ignoreActors.Add(AIPawn);
	BTParent->isRecoveringHealth = false;

	for (auto& pos : BTParent->CharClass->lightningStrikePos)
	{
		StrikePos.Add(pos->GetTargetLocation());
	}
	if (Mesh != nullptr)
	{
		if (UAnimInstance* AnimInst = Mesh->GetAnimInstance())
		{
			AIAnimator = AnimInst;
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBasicAttack");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
			}
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canLightningStrike");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, true);
			}
		}
	}

	for (int i = 0; i < 4; i++)
	{
		spawnedParticleStrikeArea.Add(UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BTParent->CharClass->strikeAreaParticle, StrikePos[i]));
		spawnedParticleStrikeArea[i]->SetWorldScale3D(FVector(4.0f, 4.0f, 2.0f));
	}


	return EBTNodeResult::InProgress;
}

void UBTBossAI_LightningStrike_Task::TickTask(UBehaviorTreeComponent& owner, uint8* node, float DeltaSeconds)
{
	Super::TickTask(owner, node, DeltaSeconds);

	timer += DeltaSeconds;

	if (timer > 1.5f)
	{
		MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canLightningStrike");
		if (MyBoolProp != NULL)
		{
			MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
		}

		for (int i = 0; i < spawnedParticleStrikeArea.Num(); i++)
		{
			spawnedParticleStrikeArea[i]->DestroyComponent();
		}
		BTParent->blackBoardCompoenent->SetValueAsBool(TEXT("canLightningStrike"), false);
		BTParent->isRecoveringHealth = false;
		FinishLatentTask(owner, EBTNodeResult::Succeeded);
	}

	if (timer > 1.2f)
	{
		if (isHit == false)
		{
			for (int i = 0; i < 4; i++)
			{
					//-------------------------Sphere trace code
					isHit = UKismetSystemLibrary::SphereTraceSingleForObjects(GetWorld(), StrikePos[i], StrikePos[i], 350.0f,objectTypesArray, false, ignoreActors, EDrawDebugTrace::None, testHit, true);
					if (isHit == true)
					{
						APawn* playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
						if (playerPawn != nullptr)
						{
							BTParent->PlayerDamage(50.0f);
						}
						return;
					}
			}
		}

	}

	if (hasCastLightning == false)
	{
		if (timer > 1.0f)
		{
			if (spawnedParticleLightning1.Num() == 0)
			{
				for (int i = 0; i < 4; i++)
				{
					spawnedParticleLightning1.Add(UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BTParent->CharClass->lightningStrikeParticle1, StrikePos[i]));
					spawnedParticleLightning2.Add(UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BTParent->CharClass->lightningStrikeParticle2, StrikePos[i]));
					spawnedParticleLightning1[i]->SetWorldScale3D(FVector(2.25f, 2.25f, 2.0f));
					spawnedParticleLightning2[i]->SetWorldScale3D(FVector(2.25f, 2.25f, 2.0f));
				}
			}

			hasCastLightning = true;
		}
	}
}