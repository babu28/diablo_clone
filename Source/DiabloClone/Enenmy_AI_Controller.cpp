// Fill out your copyright notice in the Description page of Project Settings.

#include "Enenmy_AI_Controller.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Enemy_AI.h"
#include "Kismet/GameplayStatics.h"

AEnenmy_AI_Controller::AEnenmy_AI_Controller()
{
	PrimaryActorTick.bCanEverTick = true;

	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = false;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AEnenmy_AI_Controller::OnPawnDetected);
	GetPerceptionComponent()->ConfigureSense(*SightConfig);

}

void AEnenmy_AI_Controller::BeginPlay()
{
	Super::BeginPlay();


	mainPlayer = UGameplayStatics::GetPlayerPawn(this, 0);
	thisPawn = GetPawn();
	CharClass = (AEnemy_AI*)thisPawn;
	if (GetPerceptionComponent() != nullptr)
	{
		if (thisPawn != nullptr)
		{
			//UE_LOG(LogTemp, Warning, TEXT("not null"));
			thisPawn->OnTakeAnyDamage.AddDynamic(this, &AEnenmy_AI_Controller::TakeDamage);
		}
	}

	if (USkeletalMeshComponent* Mesh = thisPawn->FindComponentByClass<USkeletalMeshComponent>())
	{
		animInstance = Mesh->GetAnimInstance();
		attackBoolProperty = FindField<FBoolProperty>(animInstance->GetClass(), "PAttack");
	}

	attackRange *= thisPawn->GetActorScale().Z;

}

void AEnenmy_AI_Controller::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);
}

void AEnenmy_AI_Controller::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (isPlayerDetected == true)
	{
		MoveToLocation(player->GetTargetLocation());

		if (canAttack == false)
		{
			if (((thisPawn->GetTargetLocation() - player->GetTargetLocation()).Size()) < attackRange)
			{
				canAttack = true;
				PlayerAttack(true);
			}
		}

		if (canAttack == true)
		{
			attackTimer += DeltaSeconds;
			StopMovement();

			//FString TheFloatStr = FString::SanitizeFloat(attackTimer);
			//GEngine->AddOnScreenDebugMessage(-1, 1.0, FColor::Red, *TheFloatStr);

			if (attackTimer > 0.45f)
			{
				PlayerAttack(false);
				canAttack = false;
				attackTimer = 0.0f;
			}
		}
	}
}

void AEnenmy_AI_Controller::OnPawnDetected(const TArray<AActor*>& DetectedPawns)
{
	if (DetectedPawns.Num() > 0)
	{
		if (isPlayerDetected == false)
		{
			for (int i = 0; i < DetectedPawns.Num(); i++)
			{
				if (DetectedPawns[i]->HasActiveCameraComponent() == true)
				{
					player = DetectedPawns[i];
					isPlayerDetected = true;
				}
			}
		}
		else
		{
			isPlayerDetected = false;
			StopMovement();
			PlayerAttack(false);
			canAttack = false;
			attackTimer = 0.0f;
		}
	}
}

void AEnenmy_AI_Controller::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (CharClass != nullptr)
	{
		isDead = CharClass->TakeDamageToHealth(Damage);
	}
}

void AEnenmy_AI_Controller::PlayerAttack(bool isAttack)
{
	//UE_LOG(LogTemp, Warning, TEXT("Attack Player"));
	if (isDead == false)
	{
		if (isAttack == true)
		{
			if (mainPlayer != nullptr)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Attack Player"));
				PLayerDamage(5);
			}
		}

		if (attackBoolProperty != NULL)
		{
			attackBoolProperty->SetPropertyValue_InContainer(animInstance, isAttack);
			MoveToLocation(thisPawn->GetTargetLocation());
		}
	}

}