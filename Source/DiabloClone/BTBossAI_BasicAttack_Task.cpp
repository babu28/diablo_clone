// Fill out your copyright notice in the Description page of Project Settings.

#include "BTBossAI_BasicAttack_Task.h"
#include "Kismet/GameplayStatics.h"

UBTBossAI_BasicAttack_Task::UBTBossAI_BasicAttack_Task()
{
	bNotifyTick = true;
}

EBTNodeResult::Type UBTBossAI_BasicAttack_Task::ExecuteTask(UBehaviorTreeComponent& owner, uint8* node)
{
	timer = 0.0f;
	hasAttacked = false;

	BTParent = Cast<AEnemy_Boss_AI_Controller>(owner.GetAIOwner());
	APawn* AIPawn = BTParent->GetPawn();

	if (USkeletalMeshComponent* Mesh = AIPawn->FindComponentByClass<USkeletalMeshComponent>())
	{
		if (UAnimInstance* AnimInst = Mesh->GetAnimInstance())
		{
			AIAnimator = AnimInst;
			FBoolProperty* MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBasicAttack");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, true);
			}
		}
	}

	return EBTNodeResult::InProgress;
}

void UBTBossAI_BasicAttack_Task::TickTask(UBehaviorTreeComponent& owner, uint8* node, float DeltaSeconds)
{
	Super::TickTask(owner, node, DeltaSeconds);
	timer += DeltaSeconds;

	if (hasAttacked == false)
	{
		if (timer > 0.4f)
		{

			APawn* playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
			if (playerPawn != nullptr)
			{
				BTParent->PlayerDamage(10.0f);
				hasAttacked = true;
			}
		}
	}

	if (timer >= 1.0f)
	{

		FBoolProperty* MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBasicAttack");
		if (MyBoolProp != NULL)
		{
			MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
		}

		FinishLatentTask(owner, EBTNodeResult::Succeeded);
	}
}