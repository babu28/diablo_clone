// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Enemy_AI.h"
#include "Enenmy_AI_Controller.generated.h"

/**
 *
 */
UCLASS()
class DIABLOCLONE_API AEnenmy_AI_Controller : public AAIController
{
	GENERATED_BODY()

public:

	AEnenmy_AI_Controller();

	virtual void BeginPlay() override;
	virtual void OnPossess(APawn* pawn) override;
	virtual void Tick(float DeltaSeconds) override;

	UFUNCTION()
		void OnPawnDetected(const TArray<AActor*>& DetectedPawns);

	UFUNCTION()
		void TakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	// has return value so that we can edit the functionality in the blueprint , no use for the return value otherwise
	UFUNCTION(BlueprintImplementableEvent)
		int PLayerDamage(float damage);

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float attackRange = 80.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AISightRadius = 1000.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AISightAge = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AILoseSightRadius = AISightRadius + 350.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AIFieldOfView = 180.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		class UAISenseConfig_Sight* SightConfig;

	bool isDead = false;

private:

	void PlayerAttack(bool isAttack);

private:

	AActor* player = nullptr;
	APawn* thisPawn = nullptr;
	AEnemy_AI* CharClass = nullptr;
	APawn* mainPlayer = nullptr;
	FBoolProperty* attackBoolProperty = nullptr;
	UAnimInstance* animInstance = nullptr;
	bool canAttack = false;
	bool isPlayerDetected = false;
	bool hasTakenDamageRecently = false;
	float attackTimer = 0.0f;
	float damageTimer = 0.0f;
	//uint8 TeamID = 10;
};