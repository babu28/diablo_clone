// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy_Boss_AI_Controller.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "BehaviorTree/BlackboardComponent.h"

AEnemy_Boss_AI_Controller::AEnemy_Boss_AI_Controller()
{
	static ConstructorHelpers::FObjectFinder<UBehaviorTree>bt(TEXT("BehaviorTree'/Game/BossEnemy/AI/Enemy_Boss_BT.Enemy_Boss_BT'"));

	if (bt.Succeeded() == true)
	{
		behaviourTree = bt.Object;
	}

	PrimaryActorTick.bCanEverTick = true;

	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component")));

	SightConfig->SightRadius = AISightRadius;
	SightConfig->LoseSightRadius = AILoseSightRadius;
	SightConfig->PeripheralVisionAngleDegrees = AIFieldOfView;
	SightConfig->SetMaxAge(AISightAge);

	SightConfig->DetectionByAffiliation.bDetectEnemies = true;
	SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
	SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

	GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
	GetPerceptionComponent()->OnPerceptionUpdated.AddDynamic(this, &AEnemy_Boss_AI_Controller::OnPawnDetected);
	GetPerceptionComponent()->ConfigureSense(*SightConfig);

	//behaviourTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("behaviourTree"));
	blackBoardCompoenent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("blackBoard"));
}

void AEnemy_Boss_AI_Controller::BeginPlay()
{
	Super::BeginPlay();
	thisPawn = GetPawn();
	RunBehaviorTree(behaviourTree);
	CharClass = (AEnemy_Boss_AI*)thisPawn;
	boss_MaxHealth = CharClass->boss_MaxHealth;
	boss_Health = boss_MaxHealth;

	if (thisPawn != nullptr)
	{
		//UE_LOG(LogTemp, Warning, TEXT("not null"));
		thisPawn->OnTakeAnyDamage.AddDynamic(this, &AEnemy_Boss_AI_Controller::TakeDamage);
	}
}

void AEnemy_Boss_AI_Controller::OnPossess(APawn* p)
{
	Super::OnPossess(p);
	blackBoardCompoenent->InitializeBlackboard(*behaviourTree->BlackboardAsset);
}

void AEnemy_Boss_AI_Controller::Tick(float delatTime)
{
	Super::Tick(delatTime);
}

void AEnemy_Boss_AI_Controller::OnPawnDetected(const TArray<AActor*>& DetectedPawns)
{
	if (DetectedPawns.Num() > 0)
	{
		if (isPlayerDetected == false)
		{
			for (int i = 0; i < DetectedPawns.Num(); i++)
			{
				if (DetectedPawns[i]->HasActiveCameraComponent() == true)
				{
					mainPlayer = DetectedPawns[i];
					isPlayerDetected = true;
					blackBoardCompoenent->SetValueAsBool(TEXT("canSeePlayer"), true);
					blackBoardCompoenent->SetValueAsObject(TEXT("MainPlayer"), mainPlayer);
				}
				else
				{
					isPlayerDetected = false;
					blackBoardCompoenent->SetValueAsBool(TEXT("canSeePlayer"), false);
				}
			}
		}
	}
}

void AEnemy_Boss_AI_Controller::TakeDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if (isRecoveringHealth == false)
	{
		if (CharClass != nullptr)
		{
			CharClass->TakeDamageToHealth(Damage);
			damageTakenCounter++;
		}

		Damage *= 0.01f;
		boss_Health -= Damage;

		// check for death condition
		if (boss_Health <= 0.0f)
		{
			UE_LOG(LogTemp, Warning, TEXT("Text, %f"), boss_Health);
			blackBoardCompoenent->SetValueAsBool(TEXT("isDead"), true);
			return;
		}

		//RecoverHealth at every 1/4 of the health bar
		if (healRecoverCounter == 0 && (boss_Health / boss_MaxHealth) < 0.75f)
		{
			blackBoardCompoenent->SetValueAsBool(TEXT("isHalfHealth"), true);
			healRecoverCounter++;
			damageTakenCounter = 0;
			unlockBeam = true;
			unlockLighningtStrike = true;
			return;
		}
		else if (healRecoverCounter == 1 && (boss_Health / boss_MaxHealth) < 0.5f)
		{
			blackBoardCompoenent->SetValueAsBool(TEXT("isHalfHealth"), true);
			healRecoverCounter++;
			return;
		}
		else if (healRecoverCounter == 2 && (boss_Health / boss_MaxHealth) < 0.25f)
		{
			blackBoardCompoenent->SetValueAsBool(TEXT("isHalfHealth"), true);
			healRecoverCounter++;
			damageTakenCounter = 2;
			return;
		}
		//Cast Beam
		if (unlockBeam == true && damageTakenCounter == 2)
		{
			blackBoardCompoenent->SetValueAsBool(TEXT("canCastBeam"), true);
			return;
		}
		//Cast lightning strike
		if (unlockLighningtStrike == true && damageTakenCounter == 3)
		{
			blackBoardCompoenent->SetValueAsBool(TEXT("canLightningStrike"), true);
			damageTakenCounter = 0;
		}
	}
}

void AEnemy_Boss_AI_Controller::setBossHealth(float health)
{
	boss_Health = health;
}