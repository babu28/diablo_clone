// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GenericTeamAgentInterface.h"
#include "Enemy_AI.generated.h"

UCLASS()
class DIABLOCLONE_API AEnemy_AI : public ACharacter , public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:	
	// Sets default values for this character's properties
	AEnemy_AI();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	bool TakeDamageToHealth(float damageValue);

	UFUNCTION(BlueprintImplementableEvent)
		int StopEnemyMoving();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual FGenericTeamId GetGenericTeamId() const override { return FGenericTeamId(10); }
	float enemyHealth = 1.0f;

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UWidgetComponent* healthBarWidget = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite , Category = "Health Bar")
		FVector healthBarPostion;

	UPROPERTY()
		class UProgressBar* ProgressBar = nullptr;

};
