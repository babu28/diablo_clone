// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy_Boss_AI.h"
#include "Components/WidgetComponent.h"
#include "Components/ProgressBar.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"



// Sets default values
AEnemy_Boss_AI::AEnemy_Boss_AI()
{
	PrimaryActorTick.bCanEverTick = true;
	bUseControllerRotationYaw = false;
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);
	healthBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("bossAIHealthBar"));
	//converting the maxvalue to fit the damagescalevalue
	boss_MaxHealth = boss_MaxHealth * 0.01f;
}

// Called when the game starts or when spawned
void AEnemy_Boss_AI::BeginPlay()
{
	Super::BeginPlay();

	boss_Health = boss_MaxHealth;

	areanCenterPos = arenaCenter->GetTargetLocation();
	UUserWidget* Widget = nullptr;
	if (healthBarWidget != nullptr)
	{
		Widget = healthBarWidget->GetUserWidgetObject();
	}
	if (Widget != nullptr)
	{
		ProgressBar = dynamic_cast<UProgressBar*>(Widget->GetWidgetFromName(FName("HealthBar")));
	}
	if (ProgressBar != nullptr)
	{
		ProgressBar->SetPercent(boss_Health);
	}
}

// Called every frame
void AEnemy_Boss_AI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isDead == true)
	{
		USkeletalMeshComponent* skeleMesh = FindComponentByClass<USkeletalMeshComponent>();
		
		deadWaitTimer += DeltaTime;
		if (deadWaitTimer > 5.0f)
		{
			UGameplayStatics::OpenLevel(GetWorld(), "GameOver");
		}
	}
}


void AEnemy_Boss_AI::TakeDamageToHealth(float Damage)
{
	Damage *= 0.01f;
	boss_Health -= Damage;
	ProgressBar->SetPercent(boss_Health / boss_MaxHealth);
	
	//check if the health goes below zero
	if (boss_Health <= 0)
	{
		isDead = true;
		if (USkeletalMeshComponent* skeleMesh = FindComponentByClass<USkeletalMeshComponent>())
		{
			if (UAnimInstance* AnimInst = skeleMesh->GetAnimInstance())
			{
				FBoolProperty* MyBoolProp = FindField<FBoolProperty>(AnimInst->GetClass(), "isDead");
				if (MyBoolProp != NULL)
				{
					MyBoolProp->SetPropertyValue_InContainer(AnimInst, true);
				}
			}
		}
	}
}

float AEnemy_Boss_AI::getCurrentBossHealth()
{
	return boss_Health;
}

void AEnemy_Boss_AI::setBossHealth(float health)
{
	boss_Health = health;
}


