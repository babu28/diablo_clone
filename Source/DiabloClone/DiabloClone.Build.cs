// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class DiabloClone : ModuleRules
{
	public DiabloClone(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule" , "UMG" , "Slate" , "GameplayAbilities", "GameplayTags", "GameplayTasks" });
    }
}
