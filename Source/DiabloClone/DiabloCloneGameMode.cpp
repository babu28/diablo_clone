// Copyright Epic Games, Inc. All Rights Reserved.

#include "DiabloCloneGameMode.h"
#include "DiabloClonePlayerController.h"
#include "DiabloCloneCharacter.h"
#include "UObject/ConstructorHelpers.h"

ADiabloCloneGameMode::ADiabloCloneGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ADiabloClonePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/MainPlayer"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}