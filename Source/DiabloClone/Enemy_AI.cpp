// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy_AI.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/ProgressBar.h"
#include "Perception/AIPerceptionComponent.h"

// Sets default values
AEnemy_AI::AEnemy_AI()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);
	healthBarWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("EnemyHealthBar"));
}

// Called when the game starts or when spawned
void AEnemy_AI::BeginPlay()
{
	Super::BeginPlay();
	healthBarWidget->SetRelativeLocation(healthBarPostion);
	UUserWidget* Widget = healthBarWidget->GetUserWidgetObject();
	if (Widget != nullptr)
	{
		ProgressBar = dynamic_cast<UProgressBar*>(Widget->GetWidgetFromName(FName("HealthBar")));
	}
	if (ProgressBar != nullptr)
	{
		ProgressBar->SetPercent(enemyHealth);
	}
}

// Called every frame
void AEnemy_AI::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemy_AI::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

bool AEnemy_AI::TakeDamageToHealth(float damageValue)
{
	damageValue *= 0.01f;
	enemyHealth -= damageValue;
	// add a knock back to the enemy on getting hit
	LaunchCharacter(FVector(0.0f , GetActorForwardVector().Y * -4000.0f , GetActorForwardVector().Z * -1000.0f), false , false);
	if (enemyHealth <= 0)
	{
		StopEnemyMoving();
		if (USkeletalMeshComponent* skeleMesh = FindComponentByClass<USkeletalMeshComponent>())
		{
			if (UAnimInstance* AnimInst = skeleMesh->GetAnimInstance())
			{
				FBoolProperty* MyBoolProp = FindField<FBoolProperty>(AnimInst->GetClass(), "isDead");
				if (MyBoolProp != NULL)
				{
					MyBoolProp->SetPropertyValue_InContainer(AnimInst, true);
				}
			}
		}
		ProgressBar->SetPercent(enemyHealth);
		return true;
	}
	ProgressBar->SetPercent(enemyHealth);
	return false;
}