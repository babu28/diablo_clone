// Fill out your copyright notice in the Description page of Project Settings.

#include "BTBossAI_Beam_Task.h"
#include "Kismet/GameplayStatics.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "particles/ParticleSystemComponent.h"

UBTBossAI_Beam_Task::UBTBossAI_Beam_Task()
{
	bNotifyTick = true;
}

EBTNodeResult::Type UBTBossAI_Beam_Task::ExecuteTask(UBehaviorTreeComponent& owner, uint8* node)
{
	//reset varibles;
	timer = 0.0f;
	beamHeadScale = 1.0f;
	spawnedParticleBeam = nullptr;
	isHit = false;

	BTParent = (AEnemy_Boss_AI_Controller*)(owner.GetAIOwner());
	AIPawn = BTParent->GetPawn();
	ignoreActors.Add(AIPawn);
	BTParent->isRecoveringHealth = true;
	Mesh = AIPawn->FindComponentByClass<USkeletalMeshComponent>();
	objectTypesArray.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));

	if (Mesh != nullptr)
	{
		if (UAnimInstance* AnimInst = Mesh->GetAnimInstance())
		{
			AIAnimator = AnimInst;
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBasicAttack");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
			}
			MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBeam");
			if (MyBoolProp != NULL)
			{
				MyBoolProp->SetPropertyValue_InContainer(AIAnimator, true);
			}
		}
	}
	spawnedParticleBeamHeadGlow = UGameplayStatics::SpawnEmitterAttached(BTParent->CharClass->beamHeadGlow, Mesh, "BeamLocation", FVector(60, 0, 0), FRotator(0, 0, 0), EAttachLocation::KeepRelativeOffset, true);
	AIPawn->SetActorLocation(BTParent->CharClass->areanCenterPos);
	return EBTNodeResult::InProgress;
}

void UBTBossAI_Beam_Task::TickTask(UBehaviorTreeComponent& owner, uint8* node, float DeltaSeconds)
{
	Super::TickTask(owner, node, DeltaSeconds);

	timer += DeltaSeconds;

	if (timer >= 6.0f)
	{
		MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "canBeam");
		if (MyBoolProp != NULL)
		{
			MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
		}

		MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "startBeam");
		if (MyBoolProp != NULL)
		{
			MyBoolProp->SetPropertyValue_InContainer(AIAnimator, false);
		}

		BTParent->blackBoardCompoenent->SetValueAsBool(TEXT("canCastBeam"), false);
		BTParent->isRecoveringHealth = false;
		spawnedParticleBeam->DestroyComponent();
		spawnedParticleBeamHeadGlow->DestroyComponent();
		spawnedParticleBeamHeadSphere->DestroyComponent();

		FinishLatentTask(owner, EBTNodeResult::Succeeded);
	}

	if (timer < 2.0f)
	{
		beamHeadScale = beamHeadScale + 0.033f;
		spawnedParticleBeamHeadGlow->SetWorldScale3D(FVector(beamHeadScale, beamHeadScale, beamHeadScale));
		return;
	}
	if (spawnedParticleBeam == nullptr)
	{
		MyBoolProp = FindField<FBoolProperty>(AIAnimator->GetClass(), "startBeam");
		if (MyBoolProp != NULL)
		{
			MyBoolProp->SetPropertyValue_InContainer(AIAnimator, true);
		}
		spawnedParticleBeam = UGameplayStatics::SpawnEmitterAttached(BTParent->CharClass->beam, Mesh, "BeamLocation", FVector(50, 0, 0), FRotator(-90, 0, 0), EAttachLocation::KeepRelativeOffset, true);
		spawnedParticleBeam->SetWorldScale3D(FVector(0.5f, 0.5f, 3.0f));
		spawnedParticleBeamHeadSphere = UGameplayStatics::SpawnEmitterAttached(BTParent->CharClass->beamHeadSphere, Mesh, "BeamLocation", FVector(100, 0, 0), FRotator(-90, 0, 0), EAttachLocation::KeepRelativeOffset, true);
		spawnedParticleBeamHeadSphere->SetWorldScale3D(FVector(0.15f, 0.15f, 0.15f));
	}

	isHit = UKismetSystemLibrary::SphereTraceSingleForObjects(GetWorld(), BTParent->CharClass->beamStart->GetTargetLocation(), BTParent->CharClass->beamEnd->GetTargetLocation(), 30.0f, objectTypesArray, false, ignoreActors, EDrawDebugTrace::None, testHit, true);
	if (isHit == true)
	{
		APawn* playerPawn = UGameplayStatics::GetPlayerPawn(this, 0);
		if (playerPawn != nullptr)
		{
			BTParent->PlayerDamage(5.0f);
			isHit = false;
		}
	}
	rotation = FQuat(FRotator(0.0f, 1.5f, 0.0f));
	AIPawn->AddActorLocalRotation(rotation);
}