// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "Player_AbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class DIABLOCLONE_API UPlayer_AbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()
	
};
