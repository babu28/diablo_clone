// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Enemy_Boss_AI.h"
#include "Enemy_Boss_AI_Controller.generated.h"

/**
 *
 */
UCLASS()
class DIABLOCLONE_API AEnemy_Boss_AI_Controller : public AAIController
{
	GENERATED_BODY()

public:

	AEnemy_Boss_AI_Controller();

	virtual void BeginPlay() override;

	virtual void OnPossess(APawn* p) override;

	virtual void Tick(float delatTime) override;

	void setBossHealth(float health);

	UFUNCTION()
		void OnPawnDetected(const TArray<AActor*>& DetectedPawns);

	UFUNCTION()
		void TakeDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		int PlayerDamage(float damage);

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		class UBlackboardComponent* blackBoardCompoenent;
	//
	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
	//	class UBehaviorTreeComponent* behaviourTreeComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		class UBehaviorTree* behaviourTree;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AISightRadius = 1300.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AISightAge = 1.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AILoseSightRadius = AISightRadius;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		float AIFieldOfView = 180.0f;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI)
		class UAISenseConfig_Sight* SightConfig;

	bool isRecoveringHealth = false;

	AEnemy_Boss_AI* CharClass = nullptr;

private:

	bool isPlayerDetected = false;
	bool unlockBeam = false;
	bool unlockLighningtStrike = false;
	int healRecoverCounter = 0.0f;
	int damageTakenCounter = 0;
	float boss_Health = 0.0f;
	float boss_MaxHealth = 2.0f;
	APawn* thisPawn = nullptr;
	AActor* mainPlayer = nullptr;

};
