// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AttributeSet.h"
#include "Player_AttributeSet.generated.h"

/**
 * 
 */
UCLASS()
class DIABLOCLONE_API UPlayer_AttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	UPROPERTY(Category = "Abilities | stats", EditAnywhere, BlueprintReadWrite)
		FGameplayAttributeData Health;
	UPROPERTY(Category = "Abilities | stats", EditAnywhere, BlueprintReadWrite)
		FGameplayAttributeData Mana;
	UPROPERTY(Category = "Abilities | stats", EditAnywhere, BlueprintReadWrite)
		FGameplayAttributeData Damage;

public:
	UFUNCTION(Category = "Attribute Funtion")
		FGameplayAttribute HeathAttribute();

	UFUNCTION(Category = "Attribute Funtion")
		FGameplayAttribute ManaAttribute();

	UFUNCTION(Category = "Attribute Funtion")
		FGameplayAttribute DamageAttribute();


	
};
