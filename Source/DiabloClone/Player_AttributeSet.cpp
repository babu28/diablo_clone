// Fill out your copyright notice in the Description page of Project Settings.


#include "Player_AttributeSet.h"

FGameplayAttribute UPlayer_AttributeSet::HeathAttribute()
{
	static UProperty* property = FindFieldChecked<UProperty>(UPlayer_AttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UPlayer_AttributeSet, Health));
	return FGameplayAttribute(property);
}

FGameplayAttribute UPlayer_AttributeSet::ManaAttribute()
{
	return FGameplayAttribute();
}

FGameplayAttribute UPlayer_AttributeSet::DamageAttribute()
{
	return FGameplayAttribute();
}
